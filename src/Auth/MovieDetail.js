import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import LoadingScreen from './LoadingScreen';
import {LOADING} from '../stores/actions/types';
import Icon from 'react-native-vector-icons/Ionicons';

const MovieDetail = ({route}) => {
  const {idMovie} = route.params;
  console.log(idMovie);
  const statusLoading = useSelector(state => state.loading);
  const dispatch = useDispatch();
  const [title, setTitle] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  useEffect(() => {
    const fetchData = async () => {
      dispatch({type: LOADING});
      const result = await axios.get(
        `https://awesome-movie-data.herokuapp.com/api/v1/movies?movieId=${idMovie}`,
      );
      setTitle(result.data.data);
      console.log('iniiiiiiiii ', result.data.data);
      dispatch({type: LOADING});
    };
    fetchData();
  }, []);
  const [input, setInput] = useState({
    title: title.title,
    description: '',
    rating: '',
  });
  const handleInputDescription = e => {
    setInput({
      ...input,
      description: e,
    });
  };
  const handleInputRating = e => {
    setInput({
      ...input,
      rating: e,
    });
  };
  const handleSubmit = e => {
    dispatch({type: LOADING});
    axios.post(
      `https://awesome-movie-data.herokuapp.com/api/v1/movies?movieId=${idMovie}`,
      input,
    );
    alert('Successfully add review!');
    dispatch({type: LOADING});
    setModalVisible(false);
  };
  return title.directors ? (
    <ScrollView style={{backgroundColor: 'white', flex: 1, display: 'flex'}}>
      <KeyboardAvoidingView behavior="position">
        <View style={styles.canvas}>
          <LoadingScreen visible={statusLoading} />

          <Text style={styles.title}>{title.title}</Text>
          <Text style={styles.title}>({title.year})</Text>

          <Image style={styles.picture} source={{uri: title.poster}} />
          <Text style={styles.subtitle}>{title.synopsis}</Text>
          <Text style={styles.subtitle}>
            Director :{' '}
            {title.directors
              ? title.directors.map(directors => directors.name + ', ')
              : false}
          </Text>
          <Text style={styles.subtitle}>
            Writers :{' '}
            {title.writers
              ? title.writers.map(writers => writers.name + ', ')
              : false}
          </Text>
          <Text style={styles.subtitle}>
            Casts :{' '}
            {title.directors
              ? title.casts.map(casts => casts.name + ', ')
              : false}
          </Text>
          {/* <TouchableOpacity
            onPress={() => setModalVisible(true)}
            style={{
              fontWeight: 'bold',
              backgroundColor: 'white',
              borderRadius: 20,
              padding: 5,
              marginTop: 10,
              marginRight: 180,
              marginLeft: 10,
              flexDirection: 'row',
            }}>
            <Icon name="md-star" size={20} color="gold" />
            <Text style={{fontSize: 15, marginLeft: 5, fontWeight: 'bold'}}>
              Add Review
            </Text>
          </TouchableOpacity>
          <Modal
            transparent={true}
            animationType="fade"
            visible={modalVisible}
            onRequestClose={() => {
              setModalVisible(false);
            }}>
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: '80%',
                  height: '70%',
                  backgroundColor: 'white',
                  borderWidth: 5,
                  borderRadius: 20,
                  borderColor: '#faa02e',
                }}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                  Add Your Review :{' '}
                </Text>
                <TextInput
                  onChangeText={value => handleInputDescription(value)}
                  style={{
                    borderWidth: 1,
                    borderRadius: 10,
                    margin: 5,
                    paddingHorizontal: 80,
                    fontWeight: '500',
                  }}
                  placeholder="Write your review"
                  placeholderTextColor="black"
                  multiline={true}
                  numberOfLines={4}
                />
                <TextInput
                  onChangeText={value => handleInputRating(value)}
                  style={{
                    borderWidth: 1,
                    borderRadius: 10,
                    margin: 5,
                    paddingHorizontal: 80,
                    fontWeight: '500',
                  }}
                  keyboardType="number-pad"
                  placeholder="Ratefrom 1-10"
                  placeholderTextColor="black"
                />

                <TouchableOpacity onPress={handleSubmit}>
                  <Text
                    style={{
                      backgroundColor: '#ecbd00',
                      borderRadius: 20,
                      color: 'black',
                      fontSize: 15,
                      fontWeight: 'bold',
                      padding: 5,
                      marginVertical: 5,
                    }}>
                    Rate Now
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => setModalVisible(false)}>
                  <Text
                    style={{
                      backgroundColor: '#ecbd00',
                      borderRadius: 20,
                      color: 'black',
                      fontSize: 15,
                      fontWeight: 'bold',
                      padding: 5,
                      marginVertical: 5,
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal> */}
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  ) : null;
};

export default MovieDetail;

const styles = StyleSheet.create({
  canvas: {
    flex: 1,
    margin: 20,
    backgroundColor: '#faa02e',
    borderRadius: 20,
    padding: 10,
  },
  title: {
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  picture: {
    width: 200,
    height: 300,
    borderRadius: 5,
    alignSelf: 'center',
    marginVertical: 20,
  },
  subtitle: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
