import * as React from 'react';
import {useState, useEffect} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  SafeAreaView,
  FlatList,
  Image,
} from 'react-native';
import axios from 'axios';
import {useSelector, useDispatch} from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import LoadingScreen from './LoadingScreen';
import {LOADING} from '../stores/actions/types';

function Item({title, desc, author, rating}) {
  // console.log(desc);
  return (
    <View style={styles.item}>
      <View style={{flexDirection: 'row'}}>
        <View>
          {/* <Image
            style={{width: 100, height: 150, borderRadius: 5}}
            source={{uri: title.poster}}
          /> */}
        </View>
        <View style={{marginLeft: 10, marginRight: 5}}>
          <Text style={styles.subtitle}>
            <Icon name="md-star" size={16} />
            {` ${rating}`}
          </Text>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.subtitle}>{desc}</Text>
          {author.map(res => (
            <View>
              {/* <Text style={styles.subtitle}>{res._id}</Text> */}
              <Text style={styles.subtitle}>{res.fullname}</Text>
            </View>
          ))}
        </View>
      </View>
      <View></View>
    </View>
  );
}

export default function MyReview(props) {
  const getUser = async () => {
    const res = await AsyncStorage.getItem('userLocal');
    setUserData(JSON.parse(res));
    await getMyReview();
  };
  const [userData, setUserData] = useState([]);
  const statusLoading = useSelector(state => state.loading);
  const dispatch = useDispatch();
  const [title, setTitle] = useState([]);
  useEffect(() => {
    getUser();
  }, []);
  // console.log(userData.image);

  const getMyReview = async () => {
    try {
      const headers = await {
        'Content-Type': 'application/json',
        Authorization: userData.token,
      };
      console.log('headers', headers);
      dispatch({type: LOADING});
      let res = await axios.get(
        `https://awesome-movie-data.herokuapp.com/api/v1/reviews?page=1`,
        {
          headers: headers,
        },
      );

      // console.log('datakoe ', res.data.data.docs);
      setTitle(res.data.data.docs);
      console.log('masuk data', title[2]._id);
      dispatch({type: LOADING});
    } catch (error) {
      console.log(error);
      dispatch({type: LOADING});
    }
  };
  return title ? (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      <LoadingScreen visible={statusLoading} />
      <Text style={{fontSize: 35, marginLeft: 20, fontWeight: 'bold'}}>
        My Review
      </Text>
      <FlatList
        data={title}
        renderItem={({item}) => (
          <Item
            title={item.title}
            desc={item.description}
            author={[item.author, item._id]}
            rating={item.rating}
          />
        )}
        keyExtractor={item => item._id}
      />
    </View>
  ) : null;
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#faa02e',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  title: {
    fontWeight: 'bold',

    fontSize: 20,
    borderBottomWidth: 2,
    marginRight: 90,
  },
  subtitle: {
    fontWeight: 'bold',
    paddingRight: 70,
  },
});
