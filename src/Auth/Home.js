import * as React from 'react';
import {useState, useEffect, useCallback} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  SafeAreaView,
  FlatList,
  Image,
  Modal,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Profile from './Profile';
import MyReview from './MyReview';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import Login from './Login';
import {useSelector} from 'react-redux';

function Item({title, nav}) {
  return (
    <TouchableOpacity
      onPress={() => {
        nav.navigate('MovieDetail', {idMovie: title._id});
      }}>
      <View style={styles.item}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Image
              style={{width: 100, height: 150, borderRadius: 5}}
              source={{uri: title.poster}}
            />
          </View>
          <View style={{marginLeft: 10, marginRight: 5}}>
            <Text style={styles.title}>
              {title.title} ({title.year})
            </Text>
            <Text style={styles.subtitle}>
              <Icon name="md-star" size={16} />
              {` ${title.rating}`}
            </Text>
            <Text style={styles.subtitle}>Cast :</Text>
            <Text style={styles.subtitle}>
              {title.casts.map(cast => cast.name + ', ')}
            </Text>
            <Text style={styles.subtitle}>Director :</Text>
            <Text style={styles.subtitle}>
              {title.directors.map(director => director.name + ', ')}
            </Text>
          </View>
        </View>
        <View>
          <Text style={{fontWeight: 'bold'}}>Synopsis: {title.synopsis}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

function HomeScreen({navigation}) {
  // const {navigation} = props;
  const [title, setTitle] = useState([]);
  const [searchData, setSearchData] = useState([]);
  const [searchInput, setSearchInput] = useState([]);
  // const [page, setPage] = useState(1);
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios.get(
        'https://awesome-movie-data.herokuapp.com/api/v1/movies/all',
      );
      setTitle(result.data.data);
      console.log('iniiiiiiiii ', result.data.data);
    };
    fetchData();
  }, []);

  const loadMore = () => {
    setPage(page + 1);
  };

  function _searchingData(text) {
    setSearchInput(text);
    const catchData = title.docs.filter(val => {
      const itemData = `${val.title.toUpperCase()}`;
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });
    setSearchData(catchData);
  }

  console.log('loop data', searchInput.length);

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      <View
        style={{
          height: 80,
          justifyContent: 'center',
          paddingHorizontal: 5,
        }}>
        <View
          style={{
            height: 50,
            backgroundColor: 'white',
            flexDirection: 'row',
            padding: 5,
            alignItems: 'center',
            borderWidth: 1,
            borderRadius: 30,
            marginLeft: 10,
            marginRight: 10,
            marginTop: 30,
          }}>
          <Icon name="ios-search" style={{fontSize: 25, marginLeft: 20}} />
          <TextInput
            placeholder="Search"
            style={{fontSize: 15, paddingLeft: 15}}
            onChangeText={text => _searchingData(text)}
          />
        </View>
        <Text style={{fontSize: 25, fontWeight: 'bold', marginLeft: 15}}>
          Genre
        </Text>
      </View>
      <View style={{flexDirection: 'row', marginTop: 20}}>
        <Text style={styles.genre}>Drama</Text>
        <Text style={styles.genre}>Thriller</Text>
        <Text style={styles.genre}>Comedy</Text>
        <Text style={styles.genre}>Action</Text>
      </View>
      <View style={{marginBottom: 140}}>
        <FlatList
          data={searchInput.length > 1 ? searchData : title.docs}
          renderItem={({item}) => <Item title={item} nav={navigation} />}
          keyExtractor={item => item._id}
        />
      </View>
    </View>
  );
}

const Tab = createBottomTabNavigator();

export default function Container() {
  const profileState = useSelector(state => state.auth);
  console.log('profilestate home', profileState);

  const getUser = async () => {
    const res = await AsyncStorage.getItem('userLocal');
    if (res) {
      setUserData(JSON.parse(res));
    }
  };
  const [userData, setUserData] = useState([]);
  if (profileState === false) {
    console.log('logout');
    if (userData.length !== 0) {
      return setUserData([]);
    }
  }
  useEffect(() => {
    // const unsubscribe = navigation.addListener('focus', async () => {
    // do something
    // console.log('nah');
    getUser();
    // });

    // return unsubscribe;
  }, []);
  if (profileState !== '') {
    console.log('login');
    if (userData.length === 0) {
      getUser();
    }
  }

  console.log('userdata', userData);
  return (
    <Tab.Navigator>
      {userData.length !== 0 ? (
        <Tab.Screen
          name="MyReview"
          component={MyReview}
          options={{
            tabBarIcon: ({tintColor}) => <Icon name="ios-paper" size={25} />,
          }}
        />
      ) : null}
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({tintColor}) => <Icon name="ios-home" size={25} />,
        }}
      />
      <Tab.Screen
        name="Profile"
        component={userData.length !== 0 ? Profile : Login}
        options={{
          tabBarIcon: ({tintColor}) => (
            <Image
              source={
                profileState
                  ? {uri: profileState.image}
                  : userData.length === 0
                  ? require('../img/profile-user.png')
                  : {uri: userData.image}
              }
              style={{
                width: 25,
                height: 25,
                position: 'absolute',
                borderRadius: 90,
              }}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#faa02e',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
  },
  title: {
    fontWeight: 'bold',

    fontSize: 20,
    borderBottomWidth: 2,
    marginRight: 90,
  },
  subtitle: {
    fontWeight: 'bold',
    paddingRight: 70,
  },
  FlatList: {
    backgroundColor: '#ecbd00',
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15,
    borderRadius: 10,
    padding: 20,
  },
  genre: {
    backgroundColor: '#ecbd00',
    borderRadius: 10,
    fontWeight: 'bold',
    fontSize: 15,
    padding: 10,
    marginLeft: 15,
    marginBottom: 10,
  },
});
