import React, {Component} from 'react';
import {Text, View, ActivityIndicator, Modal} from 'react-native';

const Loadingscreen = props => {
  console.log('params', props.visible);
  return (
    <Modal visible={props.visible} transparent={true}>
      <View
        style={{
          backgroundColor: 'rgba(191,191,191,0.3)',
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
        }}>
        <ActivityIndicator size={50} animating={true} />
      </View>
    </Modal>
  );
};

export default Loadingscreen;
