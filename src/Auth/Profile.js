import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Modal,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-community/async-storage';
import {useDispatch} from 'react-redux';
import {ACTION_CHANGE_AVATAR, ACTION_SIGN_OUT} from '../stores/actions/auth';
import {ACTION_UPDATE_PROFILE} from '../stores/actions/auth';
import LoadingScreen from './LoadingScreen';
import {useSelector} from 'react-redux';
import {SIGN_OUT} from '../stores/actions/types';

// function uploadImage() {
// }

const Profile = props => {
  const {navigation} = props;
  const statusLoading = useSelector(state => state.loading);
  const profileState = useSelector(state => state.auth);
  const dispatch = useDispatch();
  console.log('profile', profileState);
  console.log('profilestate', profileState.image);
  const [input, setInput] = useState({
    fullname: '',
  });
  const [modalVisible, setModalVisible] = useState(false);
  const handleInputFullname = e => {
    setInput({
      ...input,
      fullname: e,
    });
  };

  const handleSubmit = e => {
    e.preventDefault();

    // console.log(input.fullname);
    // console.log('handleInput');
    dispatch(ACTION_UPDATE_PROFILE(input.fullname, userData.token));
  };

  const log_out = e => {
    e.preventDefault();

    // console.log(input.fullname);
    // console.log(userData.token);
    // AsyncStorage.clear();
    dispatch({type: SIGN_OUT});
    navigation.navigate('Home');
    setUserData([]);
  };

  const uploadImage = props => {
    // console.log(userData.token);
    const options = {
      title: 'Select Avatar',
      customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
     * The first arg is the options object for customization (it can also be null or omitted for default options),
     * The second arg is the callback which sends object: response (more info in the API Reference)
     */
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = response;
        dispatch(ACTION_CHANGE_AVATAR(userData.token, source));
        getUser;
      }
    });
  };

  const getUser = async () => {
    const res = await AsyncStorage.getItem('userLocal');
    if (res) {
      setUserData(JSON.parse(res));
    } else {
      setUserData([]);
    }
  };
  const [userData, setUserData] = useState([]);
  useEffect(() => {
    getUser();
  }, []);
  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1, display: 'flex'}}>
      <KeyboardAvoidingView behavior="position">
        <LoadingScreen visible={statusLoading} />
        <View style={styles.TopSide}>
          <Image
            source={
              profileState
                ? {uri: profileState.image}
                : userData.length > 0
                ? require('../img/profile-user.png')
                : {uri: userData.image}
            }
            style={{
              width: 150,
              height: 150,
              position: 'absolute',
              borderRadius: 90,
            }}
          />
          <TouchableOpacity onPress={uploadImage}>
            <Icon name="account-edit" size={50} style={styles.ProfileIcon} />
          </TouchableOpacity>
        </View>
        <View style={styles.MiddleSide}>
          <TextInput
            style={styles.FillableContent}
            defaultValue={
              profileState ? profileState.fullname : userData.fullname
            }
            placeholderTextColor="black"
            editable={false}
          />
          <TextInput
            style={styles.FillableContent}
            defaultValue={profileState ? profileState.email : userData.email}
            placeholderTextColor="black"
            keyboardType="email-address"
            editable={false}
          />
        </View>
        <View style={styles.BottomSide}>
          <TouchableOpacity
            style={{marginBottom: 20}}
            onPress={() => setModalVisible(true)}>
            <Text style={styles.Logout}>Edit Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={log_out}>
            <Text style={styles.Logout}>Logout</Text>
          </TouchableOpacity>
        </View>
        <Image
          source={require('../img/AMP-Logo.png')}
          style={{width: 200, height: 100, alignSelf: 'center', marginTop: 50}}
        />
        <Modal
          transparent={true}
          animationType="fade"
          visible={modalVisible}
          onRequestClose={() => {
            setModalVisible(false);
          }}>
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                width: '80%',
                height: '50%',
                backgroundColor: 'white',
                borderWidth: 5,
                borderRadius: 20,
                borderColor: '#faa02e',
              }}>
              <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                Edit Your Name :{' '}
              </Text>
              <TextInput
                onChangeText={value => handleInputFullname(value)}
                style={{
                  borderWidth: 1,
                  borderRadius: 10,
                  margin: 5,
                  paddingHorizontal: 80,
                  fontWeight: '500',
                }}
                placeholder={userData.fullname}
                placeholderTextColor="black"
              />
              <TouchableOpacity onPress={handleSubmit}>
                <Text
                  style={{
                    backgroundColor: '#ecbd00',
                    borderRadius: 20,
                    color: 'black',
                    fontSize: 15,
                    fontWeight: 'bold',
                    padding: 5,
                  }}>
                  Update Profile
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default Profile;

const styles = StyleSheet.create({
  TopSide: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  MiddleSide: {
    flex: 1,
  },
  BottomSide: {
    flex: 1,
    alignContent: 'center',
    marginTop: 30,
  },
  ProfileIcon: {
    color: 'black',
    backgroundColor: 'yellow',
    borderRadius: 90,
    marginTop: 120,
    marginLeft: 90,
  },
  FillableContent: {
    borderBottomWidth: 1,
    borderBottomColor: '#faa02e',
    marginLeft: 30,
    marginRight: 30,
    fontSize: 15,
    color: 'black',
  },
  Logout: {
    backgroundColor: '#faa02e',
    textAlign: 'center',
    borderRadius: 30,
    marginLeft: 70,
    marginRight: 70,
    padding: 5,
    fontSize: 20,
    fontWeight: 'bold',
  },
  SignIn: {
    color: '#faa02e',
    textAlign: 'center',
    marginTop: 20,
    fontSize: 15,
    fontWeight: 'bold',
  },
});
