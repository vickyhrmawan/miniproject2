import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';
import {ACTION_SIGN_UP} from '../stores/actions/auth';
import LoadingScreen from './LoadingScreen';

const Register = props => {
  const statusLoading = useSelector(state => state.loading);
  const {navigation} = props;
  const dispatch = useDispatch();
  const [input, setInput] = useState({
    fullname: '',
    email: '',
    password: '',
    password_confirmation: '',
  });

  const handleInputFullname = e => {
    setInput({
      ...input,
      fullname: e,
    });
  };

  const handleInputEmail = e => {
    setInput({
      ...input,
      email: e,
    });
  };

  const handleInputPassword = e => {
    setInput({
      ...input,
      password: e,
    });
  };

  const handleInputPasswordConfirmation = e => {
    setInput({
      ...input,
      password_confirmation: e,
    });
  };

  console.log(input);

  const handleSubmit = e => {
    e.preventDefault();

    console.log('handleInput');
    dispatch(ACTION_SIGN_UP(input));
  };
  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1, display: 'flex'}}>
      <KeyboardAvoidingView behavior="position">
        <LoadingScreen visible={statusLoading} />
        <View style={styles.TopSide}>
          <Text
            style={{
              color: 'black',
              fontSize: 40,
              marginBottom: 80,
              marginTop: 40,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            Registration Form
          </Text>
        </View>
        <View style={styles.MiddleSide}>
          <TextInput
            onChangeText={value => handleInputFullname(value)}
            style={styles.FillableContent}
            placeholder="Full Name"
            placeholderTextColor="black"
          />
          <TextInput
            onChangeText={value => handleInputEmail(value)}
            style={styles.FillableContent}
            placeholder="Email"
            placeholderTextColor="black"
            keyboardType="email-address"
          />
          <TextInput
            onChangeText={value => handleInputPassword(value)}
            style={styles.FillableContent}
            placeholder="Password"
            placeholderTextColor="black"
            secureTextEntry
          />
          <TextInput
            onChangeText={value => handleInputPasswordConfirmation(value)}
            style={styles.FillableContent}
            placeholder="Password Confirmation"
            placeholderTextColor="black"
            secureTextEntry
          />
        </View>
        <View style={styles.BottomSide}>
          <TouchableOpacity onPress={handleSubmit}>
            <Text style={styles.SignUp}>Sign Up</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text style={styles.SignIn}>Already have an account? Sign In!</Text>
          </TouchableOpacity>
        </View>
        <Image
          source={require('../img/AMP-Logo.png')}
          style={{width: 200, height: 100, alignSelf: 'center'}}
        />
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default Register;

const styles = StyleSheet.create({
  TopSide: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  MiddleSide: {
    flex: 1,
  },
  BottomSide: {
    flex: 1,
    alignContent: 'center',
    marginTop: 30,
  },
  ProfileIcon: {
    color: 'black',
    backgroundColor: 'yellow',
    borderRadius: 90,
    marginTop: 120,
    marginLeft: 90,
  },
  FillableContent: {
    borderBottomWidth: 1,
    borderBottomColor: '#faa02e',
    marginLeft: 30,
    marginRight: 30,
    fontSize: 20,
    color: 'black',
  },
  SignUp: {
    backgroundColor: '#faa02e',
    textAlign: 'center',
    borderRadius: 30,
    marginLeft: 70,
    marginRight: 70,
    padding: 5,
    fontSize: 20,
    fontWeight: 'bold',
  },
  SignIn: {
    color: '#faa02e',
    textAlign: 'center',
    marginTop: 20,
    fontSize: 15,
    fontWeight: 'bold',
  },
});
