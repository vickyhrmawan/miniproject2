import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Image,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {ACTION_SIGN_IN} from '../stores/actions/auth';
import LoadingScreen from './LoadingScreen';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {LOADING} from '../stores/actions/types';

const Login = props => {
  const statusLoading = useSelector(state => state.loading);
  const {navigation} = props;
  const dispatch = useDispatch();
  const [input, setInput] = useState({
    email: '',
    password: '',
  });
  const handleInputEmail = e => {
    setInput({
      ...input,
      email: e,
    });
  };

  const handleInputPassword = e => {
    setInput({
      ...input,
      password: e,
    });
  };

  const handleSubmit = e => {
    e.preventDefault();

    console.log('handleInput');
    dispatch(ACTION_SIGN_IN(input, navigation));
    // setInput({email: '', password: ''});
    // console.log(input);
  };
  return (
    <ScrollView style={{backgroundColor: 'white', flex: 1, display: 'flex'}}>
      <KeyboardAvoidingView behavior="position">
        <LoadingScreen visible={statusLoading} />
        <View style={styles.TopSide}>
          <Image
            source={require('../img/AMP-Logo.png')}
            style={{width: 400, height: 200}}
          />
        </View>
        <View style={styles.MiddleSide}>
          <TextInput
            onChangeText={value => handleInputEmail(value)}
            style={styles.FillableContent}
            placeholder="Email"
            placeholderTextColor="black"
            keyboardType="email-address"
          />
          <TextInput
            onChangeText={value => handleInputPassword(value)}
            style={styles.FillableContent}
            placeholder="Password"
            placeholderTextColor="black"
            secureTextEntry
          />
          <Text style={styles.ForgotPassword}>Forgot Your Password?</Text>
        </View>
        <View style={styles.BottomSide}>
          <TouchableOpacity onPress={handleSubmit}>
            <Text style={styles.SignIn}>Sign In</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={styles.SignUp}>Don't have an account? Sign Up!</Text>
          </TouchableOpacity>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 20}}>
            <View
              style={{
                borderWidth: 1,
                borderColor: '#faa02e',
                flex: 1,
                justifyContent: 'center',
                marginLeft: 30,
              }}></View>
            <Text
              style={{
                marginLeft: 20,
                marginRight: 20,
                textAlign: 'center',
                color: '#faa02e',
                fontSize: 20,
              }}>
              or
            </Text>
            <View
              style={{
                borderWidth: 1,
                borderColor: '#faa02e',
                flex: 1,
                justifyContent: 'center',
                marginRight: 30,
              }}></View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginLeft: 30,
              marginRight: 30,
            }}>
            <Icon name="facebook" size={60} style={styles.MovieIcon} />
            <Icon name="google-plus" size={60} style={styles.MovieIcon} />
            <Icon name="linkedin" size={60} style={styles.MovieIcon} />
          </View>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default Login;

const styles = StyleSheet.create({
  TopSide: {
    display: 'flex',
    flex: 1,
  },
  MiddleSide: {
    flex: 1,
  },
  BottomSide: {
    flex: 1,
    alignContent: 'center',
    marginTop: 50,
  },
  Title: {
    color: '#faa02e',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
    fontFamily: 'Roboto',
  },
  MovieIcon: {
    color: '#faa02e',
    textAlign: 'center',
    marginTop: 30,
  },
  FillableContent: {
    borderBottomWidth: 1,
    borderBottomColor: '#faa02e',
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    fontSize: 20,
    color: 'black',
  },
  ForgotPassword: {
    textAlign: 'right',
    color: '#faa02e',
    fontWeight: 'bold',
    marginRight: 30,
    marginTop: 15,
    fontSize: 15,
  },
  SignIn: {
    backgroundColor: '#faa02e',
    textAlign: 'center',
    borderRadius: 30,
    marginLeft: 70,
    marginRight: 70,
    padding: 5,
    fontSize: 20,
    fontWeight: 'bold',
  },
  SignUp: {
    color: '#faa02e',
    textAlign: 'center',
    marginTop: 20,
    fontSize: 15,
    fontWeight: 'bold',
  },
});
