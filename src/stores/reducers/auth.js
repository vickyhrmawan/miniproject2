import {
  SIGN_UP,
  SIGN_IN,
  SIGN_OUT,
  ERROR_LOGIN,
  ERROR_UPDATE,
  UPDATE_PROFILE,
  CHANGE_AVATAR,
} from '../actions/types';
import AsyncStorage from '@react-native-community/async-storage';

let Data = '';

AsyncStorage.getItem('userLocal', (error, result) => {
  if (result) {
    Data = result;
  }
});

const authReducer = (state = Data, action) => {
  switch (action.type) {
    case SIGN_UP:
      AsyncStorage.setItem('userLocal', JSON.stringify(action.payload));
      // alert(action.payload.message);
      return action.payload;

    case SIGN_IN:
      AsyncStorage.setItem('userLocal', JSON.stringify(action.payload));
      return action.payload;

    case SIGN_OUT:
      AsyncStorage.clear();
      alert('You are successfully Logout');
      return false;

    case UPDATE_PROFILE:
      AsyncStorage.setItem('userLocal', [
        JSON.stringify({...state, fullname: action.payload.fullname}),
      ]);
      console.log(state, action.payload);
      alert('Data successfully updated');
      return action.payload;

    case CHANGE_AVATAR:
      AsyncStorage.setItem(
        'userLocal',
        JSON.stringify({...state, image: action.payload.image}),
      );
      console.log('string', action.payload);
      alert('Data successfully updated');
      return action.payload;

    case ERROR_LOGIN:
      AsyncStorage.clear();
      alert('Incorrect Email or Password Combination');
      return false;

    case ERROR_UPDATE:
      AsyncStorage.clear();
      alert('Your Token is Expired, please do SignIn again');
      return false;

    default:
      return state;
  }
};

export default authReducer;
