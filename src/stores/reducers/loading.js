import {LOADING} from '../actions/types';

let statusLoading = false;

const loadingReducer = (state = statusLoading, action) => {
  switch (action.type) {
    case LOADING:
      return !state;
    default:
      return state;
  }
};

export default loadingReducer;
