import {
  SIGN_UP,
  SIGN_IN,
  SIGN_OUT,
  LOADING,
  ERROR_LOGIN,
  ERROR_UPDATE,
  UPDATE_PROFILE,
  CHANGE_AVATAR,
} from './types';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
const baseUrl = 'https://awesome-movie-data.herokuapp.com/api/v1';

export const ACTION_SIGN_UP = input => {
  return dispatch => {
    console.log('ACTION_SIGN_UP');
    dispatch({type: LOADING});
    Axios.post(`${baseUrl}/users`, input)
      .then(res => {
        console.log(res.data);
        dispatch({
          type: SIGN_UP,
          payload: res.data.data,
        });
        alert('Registration success!');
        alert(res.data.message);
        dispatch({type: LOADING});
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: ERROR_LOGIN,
          payload: error,
        });
        alert('Please input all field with the correct data!');
        dispatch({type: LOADING});
      });
  };
};

export const ACTION_SIGN_IN = (input, navigation) => {
  console.log(input);
  return dispatch => {
    console.log('ACTION_SIGN_IN');
    dispatch({type: LOADING});
    Axios.post(`${baseUrl}/auth`, input)
      .then(res => {
        console.log(res);
        dispatch({
          type: SIGN_IN,
          payload: res.data.data,
        });
        dispatch({type: LOADING});
        navigation.navigate('Home');
      })
      .catch(error => {
        console.log(error);
        dispatch({
          type: ERROR_LOGIN,
          payload: error,
        });
        dispatch({type: LOADING});
      });
  };
};

export const ACTION_SIGN_OUT = () => {
  return dispatch;
  console.log('out');
  dispatch({type: LOADING});
  AsyncStorage.clear();
  dispatch({type: SIGN_OUT});
  dispatch({type: LOADING});
  navigation.navigate('Home');
};

export const ACTION_UPDATE_PROFILE = (input, token) => dispatch => {
  console.log(input);
  // // const local = JSON.parse(AsyncStorage.getItem('userLocal'));
  console.log(token);
  let setToken = {
    headers: {
      Authorization: token ? token : false,
    },
  };
  console.log(setToken);
  // console.log('ACTION_UPDATE_PROFILE', setToken);
  dispatch({type: LOADING});
  const dataUpload = new FormData();
  dataUpload.append('fullname', input);
  Axios.put(`${baseUrl}/users`, dataUpload, setToken)
    .then(res => {
      console.log(res);
      dispatch({
        type: UPDATE_PROFILE,
        payload: res.data.data,
      });
      dispatch({type: LOADING});
    })
    .catch(error => {
      console.log(error);
      // dispatch({
      //   type: ERROR_UPDATE,
      //   payload: error,
      // });
      dispatch({type: LOADING});
    });
};

export const ACTION_CHANGE_AVATAR = (tokenUser, source) => dispatch => {
  // const res = await AsyncStorage.getItem('userLocal');
  // console.log('token', local.token);
  let setToken = {
    headers: {
      accept: 'image/png',
      'Content-Type': 'multipart/form-data',
      Authorization: tokenUser ? tokenUser : false,
    },
  };
  console.log(
    'ACTION_CHANGE_AVATAR',
    // JSON.parse(tokenUser),
    // JSON.stringify(tokenUser),
    // tokenUser,
    // setToken,
  );
  const dataUpload = new FormData();
  dataUpload.append('image', {
    uri: source.uri,
    name: source.fileName,
    type: source.type,
  });
  console.log('source', source);
  console.log('token', setToken);
  console.log('input form', dataUpload);
  dispatch({type: LOADING});
  Axios.put(`${baseUrl}/users`, dataUpload, setToken)
    .then(res => {
      console.log('kktkk', res.data);
      dispatch({
        type: CHANGE_AVATAR,
        payload: res.data.data,
      });

      // console.log('imagepayload', res.data.data);
      dispatch({type: LOADING});
    })
    .catch(error => {
      console.log(error);
      // dispatch({
      //   type: ERROR_UPDATE,
      //   payload: error,
      // });
      dispatch({type: LOADING});
    });
};
