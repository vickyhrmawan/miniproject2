import * as React from 'react';
import {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Login from './Auth/Login';
import Register from './Auth/Register';
import Home from './Auth/Home';
import MovieDetail from './Auth/MovieDetail';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

function Routes() {
  const getUser = async () => {
    const res = await AsyncStorage.getItem('userLocal');
    console.log(JSON.parse(res));
  };
  useEffect(() => {
    getUser();
  }, []);
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home" headerMode="none">
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="MovieDetail" component={MovieDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Routes;
