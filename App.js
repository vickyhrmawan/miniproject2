import React from 'react';
import Routes from './src/Routes';
import {store} from './src/stores/Store';
import {Provider} from 'react-redux';

export default function App() {
  return (
    <Provider store={store}>
      <Routes />
    </Provider>
  );
}
