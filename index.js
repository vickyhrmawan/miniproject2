/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Login from './src/Auth/Login';
import Register from './src/Auth/Register';
import Profile from './src/Auth/Profile';
import Home from './src/Auth/Home';

AppRegistry.registerComponent(appName, () => App);
